"use strict";

const express = require('express');
const mongoose = require('mongoose');
const app = express();

// Déclaration d'un parser pour analyser "le corps (body)" d'une 'requête entrante avec POST  
// Permet donc d'analyser

// parse application/json
app.use(express.json());  

// parse application/x-www-form-urlencoded
// pas besoin pour cette application puisqu'on utilise 
// seulement des requêtes avec du JSON
// app.use(express.urlencoded({
//   extended: false
// }));


app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  );
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

const errorController = require('./controllers/error');

// Importe les routes
const postRoutes = require('./routes/post');
const authRoutes = require('./routes/auth');




// Utilisation des routes en tant que middleware
app.use('/auth', authRoutes);
app.use(postRoutes);


app.use(errorController.get404);

// Gestion des erreurs
// "Attrappe" les erreurs envoyé par "throw"
app.use(function (err, req, res, next) {
  console.log('err', err);
  if (!err.statusCode) err.statusCode = 500;
  res.status(err.statusCode).json({ message: err.message, statusCode: err.statusCode });
});


const PORT = 3000;
mongoose
  .connect('mongodb://127.0.0.1:27017/blog')
  .then(() => {
    app.listen(3000, () => {
      console.log('Node.js est à l\'écoute sur le port %s ', PORT);
    });
  })
  .catch(err => console.log(err));

