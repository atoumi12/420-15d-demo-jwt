"use strict";

const express = require('express');

const router = express.Router();

const postsController = require('../controllers/postsController');
const isAuth = require('../middleware/is-auth');

// / => GET
router.get('/', postsController.getPosts);

// /post/`postId => GET
router.get('/post/:postId', postsController.getPost);

// /post/post => POST
router.post('/post/', isAuth, postsController.createPost);

// /post/post => Put
router.put('/post/', isAuth, postsController.updatePost);

// /post/:postId => GET
router.delete('/post/:postId', isAuth, postsController.deletePost);

module.exports = router;
// exports.routes = router;

