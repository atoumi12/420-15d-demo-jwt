"use strict";

const express = require('express');

const authController = require('../controllers/authController');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

// /auth/login/ => POST
router.post('/login', authController.login);

// /auth/signup/ => POST
router.post('/signup', authController.signup);

// /auth/getUserStatus => GET
router.get('/getUserStatus', isAuth, authController.getUserStatus);

module.exports = router;
